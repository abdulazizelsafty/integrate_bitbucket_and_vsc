`timescale 1ns / 1ps

///////////////////////////////////////////////////////////////////////////
// Create Date: 03:55:45 10/2/2022
// Module Name: Half Adder
// Project Name: Half Adder
///////////////////////////////////////////////////////////////////////////
// This update made by Developer 1 

module HalfAdder(a,b,sum,carry);
input a,b;
output sum,carry;
xor(sum,a,b);
and(carry,a,b);
endmodule